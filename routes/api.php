<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ChatController;
use App\Events\Message;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('register', [\App\Http\Controllers\Api\AuthController::class, 'register']);
Route::post('login', [\App\Http\Controllers\Api\AuthController::class, 'login']);
Route::post('logout',[\App\Http\Controllers\Api\AuthController::class,'logout'])->middleware('auth:sanctum');



Route::Post('messages',[\App\Http\Controllers\ChatController::class,'message']);

// technician routes
// Route::apiresource('technicians', \App\Http\Controllers\Api\TechnicianController::class);

Route::middleware('auth:api')->group(function () {
    
Route::apiresource('technicians', \App\Http\Controllers\Api\TechnicianController::class);
Route::apiresource('users', \App\Http\Controllers\Api\UsersController::class);
Route::apiresource('roles', \App\Http\Controllers\RolesController::class);


});

