<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTechniciansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('technicians', function (Blueprint $table) {
            // personal details
            $table->id();
            $table->string('firstname');
            $table->string('lastname');
            $table->integer('phone');
            $table->string('email');
            $table->string('idno');
            $table->string('goodconductcopy');
            $table->string('idcopy');
            // location
            $table->string('location');
            $table->string('area');
            $table->string('house');
            // specialization
            $table->string('specialization');
            $table->string('certificatecopy');
            // skills
            $table->string('repair');
            $table->string('maintainance');
            $table->string('installation');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('technicians');
    }
}
