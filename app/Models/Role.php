<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;
     protected $guarded = [];
    // creating relationship btn roles and permissions
    // one role can have many permissions
    public function permission()
    {
        return $this->belongsToMany(Permission::class,'permission_role');
    }
}
