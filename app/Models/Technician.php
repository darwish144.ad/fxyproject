<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Technician extends Model
{
    use HasFactory;


    protected $fillable = [
        'firstname',
        'lastname',
        'phone',
        'email',
        'idno',
        'goodconductcopy',
        'idcopy',
        'location',
        'area',
        'house',
        'specialization',
        'certificatecopy',
        'repair',
        'maintainance',
        'installation',
    ];
}
