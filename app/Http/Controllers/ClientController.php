<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function index()
    {
      // return view('Client/client');
      $clients = DB::table('clients')
        ->orderBy('name','asc')
        ->get();

      dd($clients);
    }
}
