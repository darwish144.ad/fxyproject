<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
     public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:55',
            'email' => 'email|required|unique:users',
            'password' => 'required|confirmed',
            
        ]);
 
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'role_id'=>3
        ]);
       
       event(new Registered($user));

        $token = $user->createToken('authtoken');

        return response()->json(
            [
                'message'=>'user Registered successfully',
                'data'=>['token'=>$token->plainTextToken, 'user'=>$user]
            ]
        );

    }
    public function login(LoginRequest $request)
    {
        $request->authenticate();

        $token = $request->user()->createToken('authtoken');

       return response()->json(
            [
            'message'=>'Logged successfully',
            'data'=>[
                'user'=>$request->user(),
                'token'=>$token->plainTextToken
            ]
        ]
        );
    }  
     public function logout(Request $request)
    {
        $request->user()->tokens()->delete();

        return response()->json(
            [
                'message'=>'Logged out successfully'
            ]
        );
    }
    
}
