<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $users = User::all();
        return response([ 'users' => UserResource::collection($users), 'message' => 'Users Retrieved successfully'], 200);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUserRequest $request)
    {
        $data = $request->validated();

        $data['password']=bcrypt($data['password']);

        user::create($data);
        
    return response()->json([
            'success' => true,
            'message'=>'created successfully',
            'data' => $data            

        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    { 
            return response(['user' => new UserResource($user), 'message' => 'user Retrieved successfully'], 200);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request,User $user)
    {   
      $user->name = $request->name;
      $user->email = $request->email;
      if($request->password!=null)
      {
      $user->password = Hash::make($request->password);
      }
     
    // dd($user);
    $user->save();
    

      return response()->json([
            'success' => true,
            'message'=>'user updated successfully',
            'data' => $user
        ]);

       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
         $user->delete();

        return response(['message'=>'user Deleted successfully']);
    }
     public function makeAdmin(User $user)
    {
        $user->role = '1';

        $user->save();

        return response()->json([
                        'success' => true,
                        'message' => 'User made Admin successfully.',
                        'data'=>$user
                    ]);
        
    }
    public function makeTechnician(User $user)
    {
        $user->role = '2';

        $user->save();

        return response()->json([
                        'success' => true,
                        'message' => 'User made Technician successfully.',
                        'data'=>$user
                    ]);
        
    }

}
