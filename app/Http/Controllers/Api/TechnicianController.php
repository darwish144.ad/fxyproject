<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\TechnicianResource;
use App\Models\Technician;
use Illuminate\Auth\Events\Validated;
use Validator;
use Illuminate\Http\Request;

class TechnicianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $technicians = Technician::all();
        return response([ 'technicians' => TechnicianResource::collection($technicians), 'message' => 'Retrieved successfully'], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Technician $technician)
    {

        // $path = storage_path('public/' . $filename);
 
        $storePath ="public/uploads/technician/".$technician->id;
        
        if($request->hasFile('idcopy')) {
            $storagepath = $request->file('idcopy')->store($storePath);
            $fileName =  time().'.'.$request->idcopy->extension(); 
            $docs['id_copies'] = $fileName;
        }
        
        if($request->hasFile('goodconductcopy')) {
            $storagepath = $request->file('goodconductcopy')->store($storePath);
            $fileName = basename($storagepath);
            $docs['good_conduct'] = $fileName;
        }
         if($request->hasFile('certificatecopy')) {
            $storagepath = $request->file('certificatecopy')->store($storePath);
            $fileName = basename($storagepath);
            $docs['certificates'] = $fileName;
        }
         $data = $request->all();

        $validator = Validator::make($data, [
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'phone' => 'required|unique:technicians',
            'email' => 'required|email|unique:technicians',
            'idno' => 'required|unique:technicians',
            'goodconductcopy'=>'required|mimes:csv,txt,xlx,xls,pdf|max:2048',
            'idcopy'=>'required|mimes:csv,txt,xlx,xls,pdf|max:2048',
            'location'=>'required',
            'area'=>'required',
            'house'=>'required',
            'specialization'=>'required',
            'certificatecopy'=>'required|mimes:csv,txt,xlx,xls,pdf|max:2048',
            'repair'=>'required',
            'maintainance'=>'required',
            'installation'=>'required',


        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Error check your inputs.']);
        }
        $technician = Technician::create($data);

        return response(['technician' => new TechnicianResource($technician), 'message' => 'Created successfully'], 201);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Technician  $technician
     * @return \Illuminate\Http\Response
     */
    public function show(Technician $technician)
    {
    return response(['technician' => new TechnicianResource($technician), 'message' => 'Retrieved successfully'], 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Technician  $technician
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Technician $technician)
    {
            $technician->update($request->all());
        
            return response(['technician' => new TechnicianResource($technician), 'message' => 'updated successfully'], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Technician  $technician
     * @return \Illuminate\Http\Response
     */
    public function destroy(Technician $technician)
    {
        $technician->delete();

        return response(['message'=>'Deleted successfully']);
    }
}
